<?php
session_start();
require_once 'db.php'
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8"> 

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="style.css">
</head>

<body>
    <nav>
        <div class="logo">
        <h1><a href="index.php">T - B R A N D</a></h1>
        </div>

        <ul class="menu">
            <li><a href="login.php">Chat with me</a></li>
            <li><a href="login.php">Login</a></li>
        </ul>
    </nav>

    <div class="register">

        <form class="register" action="register_db.php" method="POST">

            <?php if (isset($_SESSION['error'])) { ?>
                <div class="alert"  >
                   <p ><?php
                    echo $_SESSION['error'];
                    unset($_SESSION['error']);
                    ?></p> 
                </div>
            <?php } ?>
            <?php if (isset($_SESSION['success'])) { ?>
                <div class="success" >
                   <p><?php
                    echo $_SESSION['success'];
                    unset($_SESSION['success']);
                    ?></p> 
                </div>
            <?php } ?>
            <h1>Sign up</h1>

            <div class="head">
                <hr>
                <p>Information</p>
                <hr>

            </div>
            <p>First name</p>
            <input type="text" class=textbox name="firstname" required>
            <p>Last name</p>
            <input type="text" class=textbox name="lastname" required>
            <p>Phone number</p>
            <input type="text" class=textbox name="phonenumber" required>
            <p>Username</p>
            <input type="text" class=textbox name="username" required>
            <p>Password</p>
            <input type="password" class=textbox name="password" required>
            <p>Comfirm password</p>
            <input type="text" class=textbox name="c_password" required>
            <div class="head">
                <hr>
                <p>Address</p>
                <hr>

            </div>
            <p>Address</p>
            <input type="text" class=textbox name="address" required>
            <p>Region</p>
            <input type="text" class=textbox name="region" required>
            <p>City</p>
            <input type="text" class=textbox name="city" required>
            <p>Postcode</p>
            <input type="text" class=textbox name="postcode" required>
            <div class="remember"><label><input type=checkbox> &nbsp; Remember</label> </div>
            <input class=btn-submit type=submit name="signup" value="Sign UP">
            
    </div>
        </form>
  
</body>
<footer>

</footer>

</html>
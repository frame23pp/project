
<?php
session_start();
require_once 'db.php'
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>login</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <nav>
        <div class ="logo">
         <h1><a href="index.php">T - B R A N D</a></h1>
        </div>

        <ul class = "menu">
            <li><a href="login.php">Chat with me</a></li>
            <li><a href="login.php">Login</a></li>
        </ul>
    </nav>
<div class="login">
        <form class="login"  action="login_db.php" method="POST">
        <?php if (isset($_SESSION['error'])) { ?>
                <div class="alert"  >
                   <p ><?php
                    echo $_SESSION['error'];
                    unset($_SESSION['error']);
                    ?></p> 
                </div>
            <?php } ?>
       <h1>Log in</h1>
       <p>Username</p>
       <input class="textbox" type="text" name="username" required >
       <p>Password</p>
       <input class="textbox" type="password" name="password" required>
       
       
         

        <div class="forgot">
            <label><input type="checkbox">  Keep me logged in</label>
        <label class="forgot"></label>
        <a href=#>Forgot password</a>
    </div>
        
       
       <input class="btn-submit" type="submit" value="Login" name="login">
       <label class="sign">Don't have an account yet? &nbsp;<a href="register.php">Sign up</a></label>

    
       
    </form>
</div>
    
</body>
<footer>
    
</footer>
</html>
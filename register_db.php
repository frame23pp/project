<?php
session_start();
require_once 'db.php';

if (isset($_POST['signup'])) {
    $firstname = $_POST['firstname'];
    $lastname = $_POST['lastname'];
    $phonenumber = $_POST['phonenumber'];
    $username = $_POST['username'];
    $password = $_POST['password'];
    $c_password = $_POST['c_password'];
    $address = $_POST['address'];
    $region = $_POST['region'];
    $city = $_POST['city'];
    $postcode = $_POST['postcode'];
    $urole = 'user';

    if (strlen($_POST['password']) > 20 || strlen($_POST['password']) < 5) {
        $_SESSION['error'] = 'รหัสผ่านต้องมีความยาวระหว่าง 5 ถึง 20 ตัวอักษร';
        header("location: register.php");
    } else if ($password != $c_password) {
        $_SESSION['error'] = 'รหัสผ่านไม่ตรงกัน';
        header("location: register.php");
    } else {
        try {
            $check_username = $conn->prepare("SELECT username FROM users WHERE username = :username ");
            $check_username->bindParam(":username", $username);
            $check_username->execute();
            $row = $check_username->fetch(PDO::FETCH_ASSOC);


            if ($row['username'] == $username) {

                $_SESSION['error'] = "username นี้มีอยู่ในระบบแล้ว ";
               header("location: register.php");
            }  
            else if (!isset($_SESSION['error'])) {
                $passwordHash = password_hash($password, PASSWORD_DEFAULT);
                $stmt = $conn->prepare("INSERT INTO users (firstname, lastname, phonenumber, username, password, address, region, city, postcode, urole)
                VALUES(:firstname, :lastname, :phonenumber, :username, :password, :address, :region, :city, :postcode, :urole)");
                $stmt->bindParam(":firstname", $firstname);
                $stmt->bindParam(":lastname", $lastname);
                $stmt->bindParam(":phonenumber", $phonenumber);
                $stmt->bindParam(":username", $username);
                $stmt->bindParam(":password", $passwordHash);
                $stmt->bindParam(":address", $address);
                $stmt->bindParam(":region", $region);
                $stmt->bindParam(":city", $city);
                $stmt->bindParam(":postcode", $postcode);
                $stmt->bindParam(":urole", $urole);
                $stmt->execute();
                $_SESSION['success'] = 'สมัคสมาชิกสำเร็จ &nbsp<a href="login.php">คลิ๊กเพื่อเข้าสู่ระบบ</a>';
                header("location: register.php");
                
            }
        } catch (PDOException $u) {

            echo $u->getMessage();
        }
    }

    
    // echo $firstname;
    //  echo $lastname;
    //  echo $phonenumber;
    // echo $username;
    // echo $password ;
    // echo $c_password;
    // echo$address ;
    // echo $region ;
    // echo  $city ;
    // echo $postcode ;
    // echo $urole ;
}
?>